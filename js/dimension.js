(function ($, Drupal, once) {
  Drupal.behaviors.initDimension = {
    attach(context, settings) {
      once('dimensionWrapper', 'fieldset.dimension-wrapper', context).forEach(
        (dimensionEl) => {
          Drupal.dimension.refresh($('legend', dimensionEl), settings);
          $('input', dimensionEl).on('blur', function () {
            Drupal.dimension.refresh(dimensionEl, settings);
          });
        });
    },
  };

  /**
   * Dimension methods.
   *
   * @namespace
   */
  Drupal.dimension = {
    refresh(el, settings) {
      const wrapper = $(el).closest('.dimension-wrapper');
      const id = $(wrapper).attr('dimension-id');
      let value = 1;

      for (const key in settings.dimension[id].fields) {
        value =
          value *
          parseFloat(
            $(`input[dimension-key="${key}"]`, wrapper)[0].value,
          ).toFixed(settings.dimension[id].fields[key].scale) *
          settings.dimension[id].fields[key].factor;
      }
      $('input[dimension-key="value"]', wrapper)[0].value = parseFloat(
        value,
      ).toFixed(settings.dimension[id].value.scale);
    },
  };
})(jQuery, Drupal, once);
